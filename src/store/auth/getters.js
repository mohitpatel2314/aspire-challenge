export default {
  userId(state) {
    return state.userId;
  },

  token(state) {
    return state.token;
  },
  // to check whether user is authenticated or not
  isAuthenticated(state) {
    return !!state.token;
  },

  didAutoLogout(state) {
    return state.didAutoLogout;
  },
};
