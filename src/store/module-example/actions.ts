import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { ExampleStateInterface } from './state';
import {approveUserLoan} from 'src/api/loan/loan'

const actions: ActionTree<ExampleStateInterface, StateInterface> = {
  setLoanAmount: ({ commit }, amt) => {
    commit('setLoanAmount', amt);
  },
  setLoanTerms: ({ commit }, terms) => {
    commit('setLoanTerms', terms);
  },
  setLoanPaid: ({ commit }, paid) => {
    commit('setLoanPaid', paid);
  },
  setRepaymentList: ({ commit }, list) => {
    commit('setRepaymentList', list);
  },
  setRecentTransactionList: ({ commit }, list) => {
    commit('setRecentTransactionList', list);
  },
  setLoanRemain: ({ commit }, remain) => {
    commit('setLoanRemain', remain);
  },
  approveUserLoan:async ({commit},email) =>{
      commit('setApplyLoanLoading',true)
      const loanResponse = await approveUserLoan(email)
      commit('setLoanGranted',loanResponse)
      commit('setApplyLoanLoading',false)
  }
};

export default actions;
