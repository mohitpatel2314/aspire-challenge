export interface ExampleStateInterface {
  prop: boolean;
  loanAmount:Number;
  loanTerms:Number;
  loanPaid:Number;
  loanRemain:Number;
  repaymentList:[];
  recentTransactionList:[],
  userDetails: userDetails | {},
  loanPending:boolean,
  loanGranted:boolean,
  applyLoanLoading:boolean

}
interface userDetails {
  id:string;
  email:string;
  acc_no:number;
  name:string
}

function state(): ExampleStateInterface {
  return {
    prop: false,
    loanAmount:0,
    loanTerms:1,
    loanPaid:0,
    loanRemain:0,
    repaymentList:[],
    recentTransactionList:[],
    userDetails:{
      name:'',
      email:'',
      acc_no:1234567899999
    },
    loanPending:false,
    loanGranted:false,
    applyLoanLoading:false
        
  }
};

export default state;
