import { MutationTree } from 'vuex';
import { ExampleStateInterface } from './state';
import { stat } from 'fs';

const mutation: MutationTree<ExampleStateInterface> = {
  setLoanAmount: (state, amt) => {
    state.loanAmount = amt;
  },
  setLoanTerms: (state, terms) => {
    state.loanTerms = terms;
  },
  setLoanPaid: (state, paid) => {
    state.loanPaid = paid;
  },
  setRepaymentList: (state, list) => {
    state.repaymentList = list;
  },
  updateRepaymentList:(state,index)=>{
    state.repaymentList.splice(index,1)
  },
  setRecentTransactionList: (state, newTransaction) => {
    //@ts-ignore
    state.recentTransactionList.push(newTransaction);
  },
  clearTransactionList:(state)=>{
    state.recentTransactionList=[]
  },
  setLoanRemain: (state, remain) => {
    state.loanRemain = remain;
  },
  setUserDetails:(state,payload) =>{
    state.userDetails=payload
  },
  setLoanPending:(state,payload)=>{
    state.loanPending=payload
  },
  setApplyLoanLoading:(state,payload) =>{
    state.applyLoanLoading=payload
  },
  setLoanGranted:(state,payload)=>{
    state.loanGranted=payload
  }
};

export default mutation;
