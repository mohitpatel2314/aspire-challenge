interface user {
    id: number;
    name: string;
    email: string;
  }
  
  interface ApiResponse {
    user: user;
  }
  
  //Mock req to fetch user details
  export const fetchUserDetails=(): Promise<ApiResponse> => {
    return new Promise<ApiResponse>(resolve => setTimeout(resolve, 1000))
      .then(() => {
        return {
          user: { id: 1234, name: 'Bob Smith', email: 'bob.smith@example.com', acc_no:21354567565 }
        };
      });
  }
  
  