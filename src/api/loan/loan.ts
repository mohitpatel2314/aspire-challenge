


interface loanApproved{
    approved:Boolean
}
//Mock req to Apporve loan for particular user
 export const approveUserLoan=(email:string): Promise<loanApproved> => {
    //In actual api we can test whether actually we can approve loan or not
    return new Promise<loanApproved>(resolve => setTimeout(resolve, 3000))
      .then(() => {
        return {
            approved:true
        };
      });
  }